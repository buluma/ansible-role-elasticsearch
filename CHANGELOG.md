# Changelog

## [1.0.3](https://github.com/buluma/ansible-role-elasticsearch/tree/1.0.3) (2022-05-13)

[Full Changelog](https://github.com/buluma/ansible-role-elasticsearch/compare/1.0.2...1.0.3)

**Closed issues:**

- add to tracker [\#18](https://github.com/buluma/ansible-role-elasticsearch/issues/18)

**Merged pull requests:**

- Bump buluma/gh-action-auto-merge-dependabot-updates from 1.0.3 to 1.0.4 [\#20](https://github.com/buluma/ansible-role-elasticsearch/pull/20) ([dependabot[bot]](https://github.com/apps/dependabot))
- Bump buluma/gh-action-auto-merge-dependabot-updates from 1.0.1 to 1.0.3 [\#19](https://github.com/buluma/ansible-role-elasticsearch/pull/19) ([dependabot[bot]](https://github.com/apps/dependabot))

## [1.0.2](https://github.com/buluma/ansible-role-elasticsearch/tree/1.0.2) (2022-03-06)

[Full Changelog](https://github.com/buluma/ansible-role-elasticsearch/compare/1.0.0...1.0.2)

**Fixed bugs:**

- Molecule Test Failed [\#17](https://github.com/buluma/ansible-role-elasticsearch/issues/17)
- Molecule Test Failed [\#14](https://github.com/buluma/ansible-role-elasticsearch/issues/14)
- Molecule Test Failed [\#13](https://github.com/buluma/ansible-role-elasticsearch/issues/13)
- Molecule Test Failed [\#12](https://github.com/buluma/ansible-role-elasticsearch/issues/12)
- Molecule Test Failed [\#11](https://github.com/buluma/ansible-role-elasticsearch/issues/11)
- Molecule Test Failed [\#9](https://github.com/buluma/ansible-role-elasticsearch/issues/9)
- Molecule Test Failed [\#8](https://github.com/buluma/ansible-role-elasticsearch/issues/8)
- Molecule Test Failed [\#7](https://github.com/buluma/ansible-role-elasticsearch/issues/7)
- Molecule Test Failed [\#6](https://github.com/buluma/ansible-role-elasticsearch/issues/6)
- Molecule Test Failed [\#5](https://github.com/buluma/ansible-role-elasticsearch/issues/5)
- Lint Test Failed [\#3](https://github.com/buluma/ansible-role-elasticsearch/issues/3)
- Lint Test Failed [\#2](https://github.com/buluma/ansible-role-elasticsearch/issues/2)

**Closed issues:**

- refresh repo [\#10](https://github.com/buluma/ansible-role-elasticsearch/issues/10)

**Merged pull requests:**

- Bump actions/checkout from 2 to 3 [\#16](https://github.com/buluma/ansible-role-elasticsearch/pull/16) ([dependabot[bot]](https://github.com/apps/dependabot))
- Bump actions/setup-python from 2 to 3 [\#15](https://github.com/buluma/ansible-role-elasticsearch/pull/15) ([dependabot[bot]](https://github.com/apps/dependabot))
- Main [\#4](https://github.com/buluma/ansible-role-elasticsearch/pull/4) ([buluma](https://github.com/buluma))
- Lint fix [\#1](https://github.com/buluma/ansible-role-elasticsearch/pull/1) ([buluma](https://github.com/buluma))

## [1.0.0](https://github.com/buluma/ansible-role-elasticsearch/tree/1.0.0) (2021-05-09)

[Full Changelog](https://github.com/buluma/ansible-role-elasticsearch/compare/4fbbb795442409e913dfcc40f241e3ba34da3082...1.0.0)



\* *This Changelog was automatically generated by [github_changelog_generator](https://github.com/github-changelog-generator/github-changelog-generator)*
